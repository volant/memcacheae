package Memcache;

use strict;
use warnings;

use AnyEvent::Socket;
use AnyEvent::Handle;
use String::CRC32;

sub new {
    my ($class, %opts) = @_;

    my $self = {};
    $self->{'servers'}     = $opts{'servers'};
    $self->{'connections'} = {};
    $self->{'timeout'}     = $opts{'timeout'} || 0.5;

    return bless $self, $class;
}

sub servers {
    my $self = shift;

    return $self->{'servers'};
}

sub get {
    my ($self, $key, $cb) = @_;
    $self->_request("get", "END", $key, $cb);
}

sub set {
    my $self = shift;
    $self->_request("set", "STORED", @_);
}

sub delete {
    my $self = shift;
    $self->_request("delete", "DELETED", @_);
}

sub _request {
    my $self = shift;
    my ($command, $last_word, $key, $value, $exp) = @_;

    return unless $command;
    return unless $key;

    my $cb = undef;
    if (ref $value eq 'CODE') {
        $cb = $value;
        $value = 0;
    }

    $value ||= 0;
    $exp   ||= 0;

    my $result = undef;

    ## connect
    $self->_connect($key);

    ## send & read
    my $host = $self->_get_sock_host($key);

    my $cv = AnyEvent->condvar;

    my $handle; $handle = new AnyEvent::Handle
        fh => $self->{'connections'}{$host}{'fh'},
        on_error => sub {
            my ($handle, $fatal, $msg) = @_;
            AE::log error => $msg;
            $handle->destroy;
            $cv->send;
        },
    ;

    # send some request line
    my $data_to_send = "";
    if ($command eq 'get') {
        $data_to_send = "$command $key 0\r\n";
    }
    if ($command eq 'set') {
        $data_to_send = "$command $key 0 $exp " . length($value) . "\r\n" . $value . "\r\n";
    }
    if ($command eq 'delete') {
        $data_to_send = "$command $key 0\r\n";
    }
    $handle->push_write ($data_to_send);

    # read the response line
    my $get_length = 0;

    if ($command eq 'get') {
        $handle->push_read(sub {
            my $line = $handle->rbuf;
            return undef unless $line;
            my @lines = ();
            if ($line !~ /\r\n$/) {
                return;
            } else {
                @lines = split /\r\n/, $line;
            }
            while (@lines) {
                $line = shift @lines;
                if ($line ne "END") {
                    unless ($get_length) {
                        my @ret_vals = split / /, $line;
                        $get_length = $ret_vals[3];
                        next;
                    } else {
                        $result .= $line;
                    }
                    if (length($result) < $get_length) {
                        next;
                    } else {
                        $cb->($result);
                        $cv->send;
                    }
                }
            }
        });
        return $cv;
    } else {
        $handle->push_read (sub {
            my $line = $handle->rbuf;
            return unless defined $line;
            my @lines = ();
            if ($line !~ /\r\n$/) {
                return;
            } else {
                @lines = split /\r\n/, $line;
            }
            while (@lines) {
                $line = shift @lines;
                if ($command eq 'set' || $command eq 'delete') {
                    if ($line ne $last_word) {
                        $cv->send;
                        next;
                    }
                    $result = 1;
                }
            }
            $cv->send;
        });
    }

    $cv->recv;

    return $result;
}

sub _connect {
    my $self = shift;
    my $key  = shift;

    my $host = $self->_get_sock_host($key);
    my ($ip, $port) = $host =~ /(\d+\.\d+\.\d+\.\d+):(\d+)/;

    my $connect_cv = AE::cv;
    my $fh;
    my $connect;
    $connect = tcp_connect($ip, $port,
        sub {
            ($fh) = @_;
            $self->{'connections'}{$host}{'fh'} = $fh;
            $connect_cv->send;
        },
        sub {
            return $self->{'timeout'};
        },
    );
    $self->{'connections'}{$host}{'guard'} = $connect;
    $connect_cv->recv;
}

sub _get_sock_host {
    my $self = shift;
    my $key  = shift;

    my $host = $self->_get_host_by_key($key);

}

sub _get_host_by_key {
    my $self = shift;
    my $key = shift;

    $key = ref $key eq 'ARRAY' ? $key->[0] : $key;
    my $hk = _hashfunc($key);

    my $servers_list = $self->servers();
    my $host = $servers_list->[$hk % scalar @{$servers_list}];

    return $host;
}

sub _hashfunc {
    return (crc32($_[0]) >> 16) & 0x7fff;
}

1;
