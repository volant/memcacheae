#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use FindBin qw($Bin);

BEGIN {
    $ENV{TESTING} = 1;
}

use lib $Bin."/../lib";
use Memcache;

my $hosts = {
    "127.0.0.1:11211" => 1,
    "127.0.0.2:11211" => 0,
    "127.0.0.3:11211" => 0,
};

my @test_data = qw/A 123 long_key_name B/;

my $mc = Memcache->new(servers => [ sort keys %$hosts ], timeout => 0.1 );

foreach my $key (@test_data) {
    $mc->_connect($key);

    my $host = $mc->_get_host_by_key($key);
    my $fh_status = defined $mc->{'connections'}{$host}{'fh'} ? 1 : 0;

    ok($fh_status == $hosts->{$host});
}

done_testing(4);
