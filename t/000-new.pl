#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use FindBin qw($Bin);

BEGIN {
    $ENV{TESTING} = 1;
}

use lib $Bin."/../lib";
use Memcache;

new_ok("Memcache");

done_testing(1);
