#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use FindBin qw($Bin);

BEGIN {
    $ENV{TESTING} = 1;
}

use lib $Bin."/../lib";
use Memcache;

my $mc = Memcache->new(servers => [ "127.0.0.1:11211", "127.0.0.1:11211", "127.0.0.1:11211" ] );

my $test_data = {
    A               => 21465,
    123             => 2120,
    long_key_name   => 24719,
    B               => 19152,
};

foreach my $key (keys %{$test_data}) {
    my $hv = Memcache::_hashfunc($key);
    ok($hv == $test_data->{$key});
}

done_testing(4);
