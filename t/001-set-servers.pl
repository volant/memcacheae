#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use FindBin qw($Bin);

BEGIN {
    $ENV{TESTING} = 1;
}

use lib $Bin."/../lib";
use Memcache;

my $mc = Memcache->new(servers => [ "127.0.0.1:11211" ] );

ok(scalar ($mc->servers));

done_testing(1);
