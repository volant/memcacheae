#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use FindBin qw($Bin);

BEGIN {
    $ENV{TESTING} = 1;
}

use lib $Bin."/../lib";
use Memcache;

my $mc = Memcache->new(servers => [ "127.0.0.1:11211", "127.0.0.2:11211", "127.0.0.3:11211" ] );

my $test_data = {
    A               => "127.0.0.1:11211",
    123             => "127.0.0.3:11211",
    long_key_name   => "127.0.0.3:11211",
    B               => "127.0.0.1:11211",
};

foreach my $key (keys %{$test_data}) {
    ok($test_data->{$key} eq $mc->_get_host_by_key($key));
}

ok("127.0.0.2:11211" eq $mc->_get_host_by_key([1, "ABC"]));

done_testing(5);
