#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 3;

use FindBin qw($Bin);

BEGIN {
    $ENV{TESTING} = 1;
}

use lib $Bin."/../lib";
use Memcache;

my $mc = Memcache->new(servers => [ "127.0.0.1:11211" ], timeout => 0.1 );

$mc->set("KEY1", 123, 86400);
$mc->set("KEY2", 1234, 86400);
$mc->set("KEY3", 12345, 86400);

my ($val1, $val2, $val3) = (0, 0, 0);

my $cv1 = $mc->get("KEY1", sub {
    $val1 = shift;
    return undef;
});

my $cv2 = $mc->get("KEY2", sub {
    $val2 = shift;
    return undef;
});

my $cv3 = $mc->get("KEY3", sub {
    $val3 = shift;
    return undef;
});

# $cv1->recv;
# $cv2->recv;
# $cv3->recv;

## цикл нужен из-за того, что мы не знаем, в какой последовательности придут
## результаты. если результата еще нет, ждем; в противном случае переходим дальше.
while (1) {
    unless ($val1) {
        $cv1->recv;
    }
    unless ($val2) {
        $cv2->recv;
    }
    unless ($val3) {
        $cv3->recv;
    }
    last if $val1 && $val2 && $val3;
}

ok($val1 eq "123");
ok($val2 eq "1234");
ok($val3 eq "12345");

